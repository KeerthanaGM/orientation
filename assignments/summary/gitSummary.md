**WHAT IS GITLAB**

GitLab is a complete DevOps platform, delivered as a single application. This makes GitLab unique and makes Concurrent DevOps possible, unlocking your organization from the constraints of a pieced together toolchain.Gitlab started as an open source project to help teams collaborate on software development.
GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration/continuous deployment pipeline features, using an open-source license, developed by GitLab Inc. The software was created by Ukrainians Dmitriy Zaporozhets and Valery Sizov.

<img src="extras/image.png" width=500>

**FEATURES**

By delivering new functionality at an industry-leading pace, GitLab now provides a single application for the entire software development and operations lifecycle. GitLab provides everything you need to Manage, Plan, Create, Verify, Package, Release, Configure, Monitor, Secure and Defend Applications.

**Open Source and Open Core?**

GitLab is an open source project with over 3,000 contributors maintained by GitLab Inc. You can install and self-manage GitLab Core under and MIT license or use GitLab's commercial software built on top of the open source edition with additional features. So it is called "open core." Hundreds of thousands of organizations use and contribute to GitLab. This community helps ensure the long-term viability of GitLab as they work together to release a new stable version on the 22nd of every month.

**Nearly Every Operation Is Local**

Most operations in Git need only local files and resources to operate — generally no information is needed from another computer on your network. If you’re used to a CVCS where most operations have that network latency overhead, this aspect of Git will make you think that the gods of speed have blessed Git with unworldly powers. Because you have the entire history of the project right there on your local disk, most operations seem almost instantaneous.

**COMMANDS (REQUIRED VOCABULARY) USED IN GITLAB**

- Repository
- Gitlab
- Commit
- Push
- Branch
- Merge
- Clone
- Fork

**INSTALLATION OF GIT**

Git is not defaultly installed on the computer. It is facile to install Git on the computer. The following command can be used to install the Git on the Linux.

- sudo apt-get install git

**GIT INTERNALS AND ARCHITECTURE**

The following are the states that the files can reside in -

- Modified
- Staged
- Committed

Simulataneously there are many different software codes (repository) in Git. They are -

- Workspace
- Staging
- Local Repository
- Remote Repository

The below picture depicts the different commands used to move the files into different trees of repository.

![](extras/image0.png)

**WORKFLOW OF GIT**

- Clone the repository
- Create a new Branch
- Modify files in the working trees
- Selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.
- Do a commit, so that the files in the staging area gets stored to the Local Git Repository.
- Do a push, so that the files in the Local repository gets stored permanently to Remote Git repository.

**GITLAB APPLICATIONS**

GitLab application offers functionality to automate the entire DevOps life cycle from planning to creation, build, verify, security testing, deploying, and monitoring offering high availability and replication, and scalability and available for using on-prem or cloud storage. Includes also a wiki, issue-tracking and CI/CD pipeline features.

![](extras/image6.png)








