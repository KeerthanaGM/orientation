**DOCKER**

Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

![](extras/image3.png)

**DOCKER PLATFORM**

Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allow you to run many containers simultaneously on a given host. Containers are lightweight because they don’t need the extra load of a hypervisor, but run directly within the host machine’s kernel. This means you can run more containers on a given hardware combination than if you were using virtual machines. You can even run Docker containers within host machines that are actually virtual machines!
Docker provides tooling and a platform to manage the lifecycle of your containers:

- Develop your application and its supporting components using containers.
- The container becomes the unit for distributing and testing your application.
- When you’re ready, deploy your application into your production environment, as a container or an orchestrated service. This works the same whether your production environment is a local data center, a cloud provider, or a hybrid of the two.

![](extras/image7.png)

**INSTALLATION OF DOCKER**

- It is better to install docker in Ubuntu 16.04
- Installing the new version of docker is preferrable.
- The below command is used to uninstall the older version of docker and install its new version.

    $ sudo apt-get remove docker docker-engine docker.io containerd runc

**DOCKER PHRASEOLOGIES**

- Docker
- Docker image
- Container
- Docker Hub

**BASIC COMMANDS IN DOCKER**

- docker ps
- docker start
- docker stop
- docker run
- docker rm

**COMMON OPERATIONS ON DOCKER**

Typically, you will be using docker in the given flow.

![](extras/image5.jpg)

- Download/pull the docker images that you want to work with.
- Copy your code inside the docker
- Access docker terminal
- Install and additional required dependencies
- Compile and Run the Code inside docker
- Document steps to run your program in README.md file
- Commit the changes done to the docker.
- Push docker image to the docker-hub and share repository with people who want to try your code.
